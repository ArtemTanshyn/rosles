var blockClick = document.getElementsByClassName("accActive");
var buttonElem = document.getElementById('button');
var mobBTN = document.getElementById('mobBurgerBTN');

mobBTN.addEventListener('click', showMenu);

function showMenu() {
    if(mobBTN.className === 'burgerButton') {
        mobBTN.className = 'burgerButtonActive';
    } else {
        mobBTN.className = 'burgerButton';
    }
    this.nextElementSibling.classList.toggle('down');
}


for (i = 0; i < blockClick.length; i++) {

    blockClick[i].addEventListener('click', function () {
        this.classList.toggle("accNoActive");
    });
};


buttonElem.addEventListener('click', onShowMenu);

function onShowMenu() {
    if(buttonElem.className === 'button') {
        buttonElem.className = 'active';
    } else {
        buttonElem.className = 'button';
    }
}


$('.lazy').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 3,
    slidesToScroll: 1,
    centerPadding: '10px',
    adaptiveHeight: true,
    responsive: [
        {
            breakpoint: 667,
            settings: {
                slidesToShow: 1
            }
        }
    ]
  });



  $('.portfolio').slick({
    dots: false,
    infinite: true,
    slidesToShow: 1,
    adaptiveHeight: true,
    arrows: true,
    responsive: [
        {
            breakpoint: 1920,
            settings: 'unslick'
        },
        {
            breakpoint: 667,
            settings: 'slick'
        }
    ]
  });